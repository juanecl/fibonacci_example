package fibonacci;

import java.util.ArrayList;
import java.util.List;
import java.util.*;

/**
 *
 * @author Juane
 */
public class Fibonacci {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number, first, second, i, ciclo;
        List<Integer> sucesion = new ArrayList<>();
        System.out.print("Introduce un número mayor a 1: ");
        try {
            number = sc.nextInt();
            System.out.println("Los primeros  " + number + " de la serie Fibonacci sons: ");
            first = 1;
            second = first;
            sucesion.add(first);
            for (i = 2; i <= number; i++) {
                sucesion.add(second);
                second = first + second;
                first = second - first;
            }      
            
            Iterator<Integer> itr = sucesion.iterator();
            int k = 1;
            while (itr.hasNext()) {
                Integer num = itr.next();
                System.out.print(num + " ");
                if(k == 10) { 
                    System.out.println();
                    k = 1;
                } else {
                    k++;
                }
                
            }
        } catch (Exception e) {
            System.out.println("Ocurrió un errors: " + e);
        }
    }
}
